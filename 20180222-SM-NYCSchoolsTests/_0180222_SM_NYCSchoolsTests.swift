//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import XCTest
@testable import _0180222_SM_NYCSchools

class _0180222_SM_NYCSchoolsTests: XCTestCase {
    
    var networkService:NYCService! = nil
    
    override func setUp() {
        super.setUp()
        networkService = NYCService()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testGetNYCHighSchoolsInformationAPI() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        networkService.getNYCHighSchoolsInformation { (outcome) in
            switch outcome {
            case .Value(let data):
                XCTAssertNotNil(data, "No data was downloaded.")
                ex.fulfill()
            case .Error( let e, _):
                XCTFail("error: \(e)")
            }
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func testGetNYCSchoolSATScoresAPI() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        networkService.getNYCSchoolSATScores(forDbn: "21K728") { (outcome) in
            switch outcome {
            case .Value(let data):
                XCTAssertNotNil(data, "No data was downloaded.")
                ex.fulfill()
            case .Error( let e, _):
                XCTFail("error: \(e)")
            }
        }
        
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
}
