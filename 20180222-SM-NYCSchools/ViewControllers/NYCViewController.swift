//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import UIKit
import MapKit

class NYCViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    var schoolArray: [NYCHighSchoolDataModel]?{
        didSet{
            self.tableView.reloadData()
        }
    }

    //MARK: ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "NYC High Schools"
		self.showNYCHighSchools()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func showNYCHighSchools(){
        NYCService().getNYCHighSchoolsInformation {
            [unowned self] (outcome:Outcome) in
            switch outcome {
            case .Value(let data):
                self.schoolArray = (data as! [NYCHighSchoolDataModel])
            case .Error(let e, _):
                DispatchQueue.main.async {
                    showUIAlert(title: e.localizedDescription, message: "Try Again Later!!", leftAction: nil, rightAction: nil, controller: self)
                }
            }
        }   
    }
    
}

extension NYCViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nycschoolcell", for: indexPath) as? NYCSchoolTableViewCell
		if(self.schoolArray != nil){
			cell?.schoolNameLabel.text = self.schoolArray![indexPath.row].school_name
			cell?.cityNameLabel.text = self.schoolArray![indexPath.row].city
		}
        return cell!
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.schoolArray != nil){
            return schoolArray!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "staticheadercell")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let satScoreViewController = storyboard.instantiateViewController(withIdentifier: "NYCSchoolSATViewController") as! NYCSchoolSATViewController
        satScoreViewController.dbn = self.schoolArray![indexPath.row].dbn
        satScoreViewController.schoolName = self.schoolArray![indexPath.row].school_name
        self.navigationController?.pushViewController(satScoreViewController, animated: true)
    }
    
}


