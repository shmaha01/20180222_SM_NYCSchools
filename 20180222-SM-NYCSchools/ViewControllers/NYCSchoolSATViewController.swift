//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import UIKit

class NYCSchoolSATViewController: UIViewController {

    @IBOutlet weak var tableView:UITableView!
    
    var dbn:String!
    var schoolName:String!
    
    var satScoreData: [NYCSchoolSATScoreDataModel]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    //MARK: ViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "SAT Score Details"
        self.getNYCSchoolSATScores()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getNYCSchoolSATScores(){
        NYCService().getNYCSchoolSATScores(forDbn: self.dbn){
            [unowned self] (outcome:Outcome) in
            switch outcome {
            case .Value(let data):
                self.satScoreData = (data as! [NYCSchoolSATScoreDataModel])
            case .Error(let e, _):
                DispatchQueue.main.async {
                    showUIAlert(title: e.localizedDescription, message: "Try Again Later!!", leftAction: nil, rightAction: nil, controller: self)
                }
            }
        }
    }
}

extension NYCSchoolSATViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "scorecell")

        if let satScoreData = self.satScoreData {
            if indexPath.row == 0 {
                cell?.textLabel?.text = "Math Score"
                cell?.detailTextLabel?.text = (satScoreData.count > 0) ? satScoreData[0].sat_math_avg_score : "Not Available"
            } else if indexPath.row == 1 {
                cell?.textLabel?.text = "Reading Score"
                cell?.detailTextLabel?.text = (satScoreData.count > 0) ?satScoreData[0].sat_critical_reading_avg_score : "Not Available"
            } else if indexPath.row == 2 {
                cell?.textLabel?.text = "Writing Score"
                cell?.detailTextLabel?.text = (satScoreData.count > 0) ?satScoreData[0].sat_writing_avg_score : "Not Available"
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 //showing only three subject scores
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var cell =  tableView.dequeueReusableCell(withIdentifier: "staticheadercell")
        if (cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                   reuseIdentifier: "staticheadercell")
        }
       if let satScoreData = self.satScoreData {
                cell?.textLabel?.text = (satScoreData.count > 0) ? satScoreData[0].school_name : self.schoolName
        }else {
            cell?.textLabel?.text = "No data available for dnb: \(self.dbn!)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
}



