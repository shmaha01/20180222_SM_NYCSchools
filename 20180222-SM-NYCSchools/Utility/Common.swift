//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import Foundation
import UIKit

//: Decodable Extension
extension Decodable {
    static func decode(data: Data) throws -> Self {
        let decoder = JSONDecoder()
        return try decoder.decode(Self.self, from: data)
    }
}

//: Encodable Extension
extension Encodable {
    func encode() throws -> Data {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        return try encoder.encode(self)
    }
}

func showUIAlert(title:String, message:String, leftAction:UIAlertAction?, rightAction: UIAlertAction?, controller:UIViewController){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    if (leftAction == nil && rightAction == nil){
        let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil)
         alert.addAction(cancelAction)
    }
    if let leftAction = leftAction {
        alert.addAction(leftAction)
    }
    if let rightAction = rightAction {
        alert.addAction(rightAction)
    }
    controller.present(alert, animated: true, completion: nil)
}
