//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import Foundation

public struct NYCHighSchoolDataModel: Decodable {
    let dbn:String
    let city: String
    let school_name:String
}
