//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import Foundation

public struct NYCSchoolSATScoreDataModel: Decodable {
    let sat_critical_reading_avg_score:String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
    let school_name: String
}
