//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import Foundation

private let NYCRootURL = "https://data.cityofnewyork.us/resource/"

struct NYCService {
    
    let networkRouter:NYCNetworkRouter
    
    init() {
        self.networkRouter = AlamofireRequestRouter()
    }
    
    func getNYCHighSchoolsInformation(completionHandler: @escaping (Outcome<AnyObject>)->Void) {
        let url = NYCRootURL+"97mf-9njv.json"
        self.networkRouter.makeRequest(url: url, resource: nil, completionHandler:{
            (outcome) -> Void in
            switch outcome{
            case .Value(let v):
                do{
                    let dataModel = try [NYCHighSchoolDataModel].decode(data: v as! Data)
                    completionHandler(Outcome.Value(dataModel as AnyObject))
                }
                catch {
                    print(error.localizedDescription)
                }
            case .Error(let e, _):
                print (e.debugDescription)
                completionHandler(Outcome.Error(e, nil))
            }
            
        })
    }
    
    func getNYCSchoolSATScores(forDbn: String, completionHandler: @escaping (Outcome<AnyObject>)->Void) {
        let url = NYCRootURL+"734v-jeq5.json?dbn="+forDbn
        self.networkRouter.makeRequest(url: url, resource: nil, completionHandler:{
            (outcome) -> Void in
            switch outcome{
            case .Value(let v):
                do{
                    let dataModel = try [NYCSchoolSATScoreDataModel].decode(data: v as! Data)
                    completionHandler(Outcome.Value(dataModel as AnyObject))
                }
                catch {
                    print(error.localizedDescription)
                }
            case .Error(let e, _):
                print (e.debugDescription)
                completionHandler(Outcome.Error(e, nil))
            }
            
        })
    }
    
}
