//
//  Created by M, Shwetha
//  Copyright © 2018. All rights reserved.
//

import UIKit

class NYCSchoolTableViewCell: UITableViewCell {

    @IBOutlet var schoolNameLabel:UILabel!
    @IBOutlet var cityNameLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
